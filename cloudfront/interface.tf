variable "origin_domain_name" {
  type        = string
  description = "Domain name of origin for distribution"
}

variable "public_facing_domain_name" {
  type        = string
  description = "Domain name of website"
}

variable "dns_zone_id" {
  type        = string
  description = "Zone ID to which the A records will be added"
}

variable "viewer_request_lambda_arn" {
  type        = string
  description = "Lambda@Edge function for viewer requests"
}

output "id" {
  value = aws_cloudfront_distribution.s3_web_distribution.id
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.s3_web_distribution.hosted_zone_id
}

output "domain_name" {
  value = aws_cloudfront_distribution.s3_web_distribution.domain_name
}
