locals {
  s3_origin_id = "s3_origin_id"
}

resource "aws_acm_certificate" "public_facing" {
  domain_name               = var.public_facing_domain_name
  subject_alternative_names = ["www.${var.public_facing_domain_name}"]
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation_records" {
  for_each = {
    for dvo in aws_acm_certificate.public_facing.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.dns_zone_id
}

resource "aws_acm_certificate_validation" "public_facing_cert_validation" {
  certificate_arn         = aws_acm_certificate.public_facing.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation_records : record.fqdn]
}

resource "aws_cloudfront_distribution" "s3_web_distribution" {
  origin {
    domain_name = var.origin_domain_name
    origin_id   = local.s3_origin_id
  }

  aliases             = [var.public_facing_domain_name, "www.${var.public_facing_domain_name}"]
  default_root_object = "index.html"
  enabled             = true

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = local.s3_origin_id
    default_ttl            = 3600
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = var.viewer_request_lambda_arn
      include_body = false
    }

  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.public_facing.arn
    ssl_support_method  = "sni-only"
  }
}
