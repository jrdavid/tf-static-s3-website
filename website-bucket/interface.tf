variable "domain" {
  type        = string
  description = "Name use for bucket name and DNS A record"
}

variable "error_document" {
  type        = string
  default     = "error.html"
  description = "Document to return in case of error"
}

variable "log_bucket_id" {
  type        = string
  description = "ID of the destination S3 bucket for logs"
}

variable "endpoint" {
  type        = string
  description = "Name of this endpoint"
}

variable "commit" {
  type        = string
  description = "Version control identifier"
}

output "endpoint" {
  value = aws_s3_bucket.bucket.website_endpoint
}

output "website_domain" {
  value = aws_s3_bucket.bucket.website_domain
}

output "bucket_domain_name" {
  value = aws_s3_bucket.bucket.bucket_domain_name
}

output "hosted_zone_id" {
  value = aws_s3_bucket.bucket.hosted_zone_id
}
