resource "aws_s3_bucket" "bucket" {
  bucket = var.domain
  acl    = "public-read"
  policy = <<EOF
{
	"Version":"2012-10-17",
	"Statement":[{
		"Sid":"PublicReadGetObject",
		"Effect":"Allow",
		"Principal": "*",
		"Action":["s3:GetObject"],
		"Resource":["arn:aws:s3:::${var.domain}/*"]
	}
	]
}
EOF
  website {
    index_document = "index.html"
    error_document = var.error_document
  }
  logging {
    target_bucket = var.log_bucket_id
    target_prefix = "${var.domain}/"
  }
}
