resource "aws_iam_role" "role_for_lambda" {
  name               = "role_for_lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com",
          "edgelambda.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole",
      "Sid": ""
    }
  ]
}
EOF
}

data "archive_file" "rewrite_trailing_slash_url" {
  type        = "zip"
  source_file = "${path.module}/rewrite_trailing_slash_url.py"
  output_path = "${path.module}/rewrite_trailing_slash_url.zip"
}

resource "aws_lambda_function" "rewrite_trailing_slash_url" {
  description      = "Append 'index.html' to URL's that end with a slash"
  filename         = data.archive_file.rewrite_trailing_slash_url.output_path
  source_code_hash = data.archive_file.rewrite_trailing_slash_url.output_base64sha256
  function_name    = "rewrite_trailing_slash_url"
  role             = aws_iam_role.role_for_lambda.arn
  handler          = "rewrite_trailing_slash_url.lambda_handler"
  runtime          = "python3.8"
  publish          = true
}
