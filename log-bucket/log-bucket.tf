resource "aws_s3_bucket" "log_bucket" {
  bucket = var.name
  acl    = "log-delivery-write"
}
