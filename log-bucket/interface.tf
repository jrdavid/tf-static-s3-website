variable "name" {
  type        = string
  description = "Name of the log bucket"
}

output "id" {
  value = aws_s3_bucket.log_bucket.id
}
