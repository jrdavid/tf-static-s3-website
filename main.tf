resource "aws_route53_zone" "toplevel" {
  name = var.domain
}

module "log_bucket" {
  source = "./log-bucket"
  name   = "log.${var.domain}"
}

module "production" {
  source         = "./website-bucket"
  endpoint       = "production"
  domain         = var.domain
  error_document = var.error_document
  log_bucket_id  = module.log_bucket.id
  commit         = var.commit
}

module "staging" {
  source         = "./website-bucket"
  endpoint       = "staging"
  domain         = "staging.${var.domain}"
  error_document = var.error_document
  log_bucket_id  = module.log_bucket.id
  commit         = var.commit
}

module "dev" {
  source        = "./website-bucket"
  endpoint      = "dev"
  domain        = "dev.${var.domain}"
  log_bucket_id = module.log_bucket.id
  commit        = var.commit
}

module "cdn_staging" {
  source                    = "./cloudfront"
  origin_domain_name        = module.staging.bucket_domain_name
  public_facing_domain_name = "staging.${var.domain}"
  dns_zone_id               = aws_route53_zone.toplevel.zone_id
  viewer_request_lambda_arn = aws_lambda_function.rewrite_trailing_slash_url.qualified_arn
}

module "cdn_production" {
  source                    = "./cloudfront"
  origin_domain_name        = module.production.bucket_domain_name
  public_facing_domain_name = var.domain
  dns_zone_id               = aws_route53_zone.toplevel.zone_id
  viewer_request_lambda_arn = aws_lambda_function.rewrite_trailing_slash_url.qualified_arn
}

resource "aws_route53_record" "production" {
  name    = var.domain
  type    = "A"
  zone_id = aws_route53_zone.toplevel.zone_id

  alias {
    name                   = module.cdn_production.domain_name
    zone_id                = module.cdn_production.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www_production" {
  name    = "www.${var.domain}"
  type    = "CNAME"
  records = [var.domain]
  ttl     = "60"
  zone_id = aws_route53_zone.toplevel.zone_id
}

resource "aws_route53_record" "staging" {
  name    = "staging.${var.domain}"
  type    = "A"
  zone_id = aws_route53_zone.toplevel.zone_id

  alias {
    name                   = module.cdn_staging.domain_name
    zone_id                = module.cdn_staging.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www_staging" {
  name    = "www.staging.${var.domain}"
  type    = "CNAME"
  records = ["staging.${var.domain}"]
  ttl     = "60"
  zone_id = aws_route53_zone.toplevel.zone_id
}

resource "aws_route53_record" "dev" {
  name    = "dev.${var.domain}"
  type    = "A"
  zone_id = aws_route53_zone.toplevel.zone_id

  alias {
    name                   = module.dev.website_domain
    zone_id                = module.dev.hosted_zone_id
    evaluate_target_health = false
  }
}
