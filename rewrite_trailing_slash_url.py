def lambda_handler(event, context):
    request = event['Records'][0]['cf']['request']
    if request['uri'].endswith("/"):
        request['uri'] += "index.html"
    return request
