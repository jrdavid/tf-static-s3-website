variable "domain" {
  type        = string
  description = "Top-level domain for website"
}

variable "error_document" {
  type        = string
  default     = "error.html"
  description = "Document to return in case of error"
}

variable "commit" {
  type        = string
  description = "Version control identifier"
}

output "dns_zone_id" {
  value       = aws_route53_zone.toplevel.zone_id
  description = "AWS DNS zone id for re-use in caller for other purposes"
}

output "name_servers" {
  value       = aws_route53_zone.toplevel.name_servers
  description = "List of name servers for newly created DNS zone"
}

output "production_endpoint" {
  value       = module.production.endpoint
  description = "HTTP endpoint for production site"
}

output "staging_endpoint" {
  value       = module.staging.endpoint
  description = "HTTP endpoint for site staging"
}

output "dev_endpoint" {
  value       = module.dev.endpoint
  description = "HTTP endpoint for develpment branches"
}

output "staging_distribution_id" {
  value       = module.cdn_staging.id
  description = "ID of the staging Cloudfront Distribution"
}

output "production_distribution_id" {
  value       = module.cdn_production.id
  description = "ID of the production Cloudfront Distribution"
}

output "staging_cdn_url" {
  value = module.cdn_staging.domain_name
}

output "production_cdn_url" {
  value = module.cdn_production.domain_name
}
